// console.log("Hi")

// Section: Document Object Model
	// allow us to be able to access or modify the properties of an html element in a webpage.
	// it is a standard on how to get, change, add, or delete HTML elements.
	// we will focus on using DOM for managing forms.

	// For selecting HTML elements we will be using the document.querySelector
	// Syntax: document.querySelector("htmlElement")
		// document refers to the whole page
		// ".querySelector" is used to select a specific object (HTML elements) from the document(web page).

		// The query selector function takes a string input that is formatted like a CSS selector when applying the styles.

	// Allternatively, we can use getElement functions to retrieve the elements.
		// document.getElement ById("txt-firs-name")

		// However, using these functions require us to identify beforehand how we get the elements. with querySelector, we can be flexible in how to retrieve the elements.

		const txtFirstName = document.querySelector("#txt-first-name");

		const txtLastName = document.querySelector("#txt-last-name");

		const spanFullName = document.querySelector("#span-full-name");

		const button = document.querySelector("#text-color");


// Section: Event Listeners
	// wheneve a user interacts with a webpage, this action is considered as an event.
	// Working with events is large part of creating interactivity in a webpage
	//to perform an action when an event triggers, you first need to listen to it.

// The function use is "addEventListener" that takes two arguments:
	// A string identifying an event
	// and a function that the listener will execute once the "specified event" is triggered.

	txtFirstName.addEventListener("keyup", (event) => {
		console.log(event.target); //contains the element
		console.log(event.target.value); // contains the actual valu
	});

	/*txtFirstName.addEventListener("keyup", (event)=>{

		// The "innerHTML" property sets or returns the HTML content (inner HTML) of an element (dv, span, etc.)
		//return the value of the attribute (.value)
		spanFullName.innerHTML = `${txtFirstName.value}`;
	})*/

	// Creating Multiple events that will trigger a same function. 

	const fullName = ()=> {
		spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
	}



	txtFirstName.addEventListener("keyup", fullName);
	txtLastName.addEventListener("keyup", fullName);
	
	button.addEventListener("click", () => {
		let color = button.value;
		spanFullName.style.color = color;
		console.log(color)
	})




